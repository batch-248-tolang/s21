console.log('Hello World');

// Array
// Arrays are used to store multiple related data/values in a single variable
// It is created/declared using [] brackets also known as "Array Literals"

let hobbies = ["cycling", "workout", "watching youtube"];

// Arrays make it easy to manage, manipulate a set of data
// Arrays have different methods/functions that allows us to manage our array

// Methods are functions associated with an object
// arrayas are actually a special type of object

console.log(typeof hobbies);

let grades =[75.4, 98.6, 90.34, 91.50];
const planets = ["Mercury", "Venus", "Mars", "Earth"];

// alternative way to write arrays
let myTasks = [
		"drink",
		"eat javascript",
		"inhale css",
		"bake react"
	];

// Arrays as a best practice contains values of the same type

let arraySample = ["Saitama", "One Punch Man", 25000, true];

// however, since there are no problems in creating arrays like this, you may encounter exceptions to this rule in the future and in fact in other JS libraries or frameworks

// array as a collection of data, has methods to manipulate and manage the array
// having values with different data types might interfere or conflict with the methods of an array

/*
	Mini Activity
	1. Create a variable that will store an array of at least 5 of your daily routing or task
	2. Create a variable which will store at least 4 capital cities in the world
	3. Log the variables in the console and send a ss in our hangouts
*/

let myDailyRoutine = [
		"Watch yt vids",
		"workout",
		"eat",
		"pre for training",
		"pre for work"
	];

let fourCapitalCities = [
		"Riyahd",
		"Manila",
		"Zurich",
		"Beijing",
	];

console.log(myDailyRoutine);
console.log(fourCapitalCities);

// each item in an array is called an element
// array as a collection of data, as a convetion, its name is usually plural

let username1 = "pink_princess";
let username2 = "DareAngel";
let username3 = "WonderfulEgg";
let username4 = "mahalparinkita_123";

let guildMembers = [username1, username2, username3, username4];

console.log(guildMembers);

// .lenght property
// the .lenght property of an array tells about the number of elements in the array
// it can actually be also be set and manipulated
// the .lenght property of an array is a number type

console.log(myDailyRoutine.length);// 5
console.log(fourCapitalCities.length);// 4

// in fact, even strings have a .length property, which tells us the number of characters in a string
// strings are able to use some array methods and properties
// white spaces are counter as characters

let fullName = "Cardo Dalisay";
console.log(fullName.length);// 13

// we can manipulate the .length property of an array, Being that .length property is a number that tells the total number of element in an array
// we can also delete the last item in an array by manipulating the .length property

myDailyRoutine.length = myDailyRoutine.length-1;
console.log(myDailyRoutine.length);
console.log(myDailyRoutine);

// we could also decrement the .length property of an array

fourCapitalCities.length--;
console.log(fourCapitalCities);

// can we do the same trick with a string?
// No

fullName.length =fullName.length-1
console.log(fullName); //Cardo Dalisay

// Can we also add or lengthen using the same trick?

let theBeatles = ["John", "Paul", "Ringp", "George"];
theBeatles.length++;
console.log(theBeatles); // 'John', 'Paul', 'Ringp', 'George', empty

theBeatles[4]= "Cardo";
console.log(theBeatles);

theBeatles[theBeatles.length]="Ely"; //theBeatles[5]= "Ely" // .length = 5
console.log(theBeatles);

theBeatles[theBeatles.length]= "Chito"; //theBeatles[6]
theBeatles[theBeatles.length]= "MJ"; //theBeatles[7]

console.log(theBeatles); //'John', 'Paul', 'Ringp', 'George', 'Cardo', 'Ely', 'Chito', 'MJ'
console.log(theBeatles.length);// theBeatles[8]

// if we want to access a particualar item in the array, we can do so with array indices. Each item are ordered according to their index
// Note: Index are number types

console.log(fourCapitalCities[0]); // first on the list = Riyahd

let lakerLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakerLegends[1]); //Shaq
console.log(lakerLegends[3]); //Magic

// we can also save/store a particular array element in a variable

let currentLaker = lakerLegends[2];
console.log(currentLaker);

// we can also update or re-assign the array elements using their index

lakerLegends[2]= "Pau Gasol";
console.log(lakerLegends);

let favoriteFoods = [
		"Tonkatsu",
		"Adobo",
		"Pizza",
		"Lasagna",
		"Sinigang"
	];

console.log(favoriteFoods);

/*
	Mini Activity
	Update and re-assign the last two items in the array with your own personal faves
	Log the favoriteFoods array in the console with the UPDATED elements using their index number. Send an ss in our hangouts

*/

favoriteFoods[3]= "Sisig";
favoriteFoods[4]= "Lechon";
console.log(favoriteFoods);

/*
	Mini Activity 3
*/

let theTrainers = ["Ash"];

function addTrainers(trainer){
	theTrainers[theTrainers.length]=trainer;
}
addTrainers("Misty");
addTrainers("Brock");
console.log(theTrainers);

/*
	Mini Activity 4
*/

function findBlackMamba(index){
	return lakerLegends[index];
}

let blackMamba = findBlackMamba(0);
console.log(blackMamba); //Kobe

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;
console.log(lastElementIndex); // 4
console.log(bullsLegends.length); // 5

console.log(bullsLegends[lastElementIndex]);// Kukoc
console.log(bullsLegends[bullsLegends.length-1]);// Kukoc

// Adding items in our array

let newArr = [];
console.log(newArr[0]);// undefined

newArr[0] = "Cloud 9";
console.log(newArr);

console.log(newArr[1]);//undefined
newArr[1] = "Fifa Champ";
console.log(newArr);

newArr[1] = "Claud Grierre";
console.log(newArr);

newArr[newArr.length] = "Leo Messi";
console.log(newArr);

newArr[newArr.length-1] = "Fifa Champ";

newArr[newArr.length-1] = "Fifa Champ Again?";
console.log(newArr);

// looping over an array
// we can loop over an array and iterate all items in the array
// set counter as the index and set a condition that as the current index iterated is less than the length of the array, we will run the loop
// it is set this way because the index starts at 0

for(let index=0; index<newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5, 12 , 30, 46, 40];
for(let index=0; index<newArr.length; index++){
	if(numArr[index] %5 ===0){
	console.log(newArr[index] + " is divisible by 5");
	}else{
		console.log(numArr[index] + " is not divisible by 5")
	}
}


let chessBoard = [

	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4'],
	['a5','b5','c5','d5','e5','f5','g5','h5'],
	['a6','b6','c6','d6','e6','f6','g6','h6'],
	['a7','b7','c7','d7','e7','f7','g7','h7'],
	['a8','b8','c8','d8','e8','f8','g8','h8']
];

console.log(chessBoard);

// access elements in a multidimensional array
console.log(chessBoard[1][4]);

console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);